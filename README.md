Polls
===== 

Technology stack for the application:

- Create-react-app (not ejected)
- React 16+
- React hooks
    - useOffline: Online/offline detection
    - usePolls: Questions API usage
- React router
- Simple date/time formatting library (dateformat)
- Axios
- NodeJS v12.14.1 or later

# Setup

- Use yarn/npm to install dependencies.
 
```bash
yarn
```
```
npm i
```


# Run

### Local

Run application on your local http://localhost:3000

```bash
yarn start
```
```bash
npm start
```

### Live

https://polls-sigma.vercel.app/

- You can vote by using radio button on the right hand side of question details (/questions/:id) and then click Save vote
- You might need to vote once to see proper percentages if API retuns invalid choices
- You need to refresh details after Save vote (you can hit refresh button or go home and come back)
- Online/Offline indicator should be shown on questions list page

# Test

Run unit tests with Jest.

```bash
yarn test
```

# Lint

ESLint command below should work. You can also add `--fix` to automatically fix errors.

```bash
yarn lint
```

#### Browser support

async/await is part of ECMAScript 2017 and is not supported in Internet Explorer and older browsers.

For older browsers fetch + polyfill library could be used.
