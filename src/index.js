import React from 'react';
import ReactDOM from 'react-dom';
import { AppRouter, PollsProvider } from './components';
import './styles/tailwind.css';

ReactDOM.render(
  <React.StrictMode>
    <PollsProvider>
      <AppRouter />
    </PollsProvider>
  </React.StrictMode>,
  document.getElementById('app')
);
