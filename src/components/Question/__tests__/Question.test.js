import React from 'react';
import { render } from '@testing-library/react';
import Question from '../Question';

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  Link: ({ children }) => children,
}));

describe('<Question />', () => {
  const renderComponent = () =>
    render(<Question published_at={new Date(2020, 0, 1)} question="MY QUESTION" />);

  test('renders properly', () => {
    const { getByText } = renderComponent();
    expect(getByText('MY QUESTION')).toBeInTheDocument();
    expect(getByText('1st Jan 2020 00:00')).toBeInTheDocument();
  });
});
