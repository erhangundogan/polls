import React from 'react';
import dateFormat from 'dateformat';
import { Link } from 'react-router-dom';

const Question = ({ question, published_at, url, choices }) => (
  <div className="question bg-white shadow rounded p-2">
    <Link to={url}>
      <div className="title font-bold">{question}</div>
      <div className="published-at">{dateFormat(published_at, 'dS mmm yyyy HH:MM')}</div>
      <div className="choices">{choices && choices.length}</div>
    </Link>
  </div>
);

export default Question;
