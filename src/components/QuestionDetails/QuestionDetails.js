import React, { useContext, useEffect, useState } from 'react';
import { useRouteMatch } from 'react-router';
import { Link } from 'react-router-dom';
import { PollsContext } from '..';

const QuestionDetail = () => {
  const {
    params: { questionId },
  } = useRouteMatch();
  const [question, setQuestion] = useState();
  const [totalVotes, setTotalVotes] = useState(0);
  const [vote, setVote] = useState();
  const { getQuestion, postChoice } = useContext(PollsContext);

  useEffect(() => {
    const getQuestionAsync = async (id) => {
      if (!isNaN(id) && getQuestion) {
        const qid = `/questions/${id}`;
        const result = await getQuestion(qid);
        setQuestion(result);
      }
    };

    getQuestionAsync(questionId);
  }, [questionId, getQuestion]);

  useEffect(() => {
    if (question) {
      const total = question.choices.reduce((acc, cur) => acc + cur.votes, 0);
      setTotalVotes(total);
    }
  }, [question]);

  if (!question) {
    return (
      <div className="question-details p-2">
        <h1 className="text-4xl">Question Details</h1>
        <div className="">Loading...</div>
      </div>
    );
  }

  const getPercent = (votes) => ((votes / totalVotes) * 100).toFixed(2);

  return (
    <div className="question-details p-2">
      <h1 className="text-4xl">Question Details</h1>
      <h2 className="text-2xl font-bold">{question.question}</h2>

      {question.choices && question.choices.length > 0 ? (
        <>
          <table className="table-auto mt-2 w-full">
            <tbody>
              {question.choices.map((details, index) => (
                <tr className={index % 2 > 0 ? 'bg-gray-200' : 'bg-white'} key={details.choice}>
                  <td className="border px-3 py-2">{details.choice}</td>
                  <td className="border px-3 py-2">{details.votes}</td>
                  <td className="border px-3 py-2">{getPercent(details.votes)} %</td>
                  <td className="border px-3 py-2">
                    <label className="cursor-pointer">
                      <input
                        type="radio"
                        name="vote"
                        value={details.choice}
                        onClick={() => setVote(details.url)}
                      />
                      <span className="ml-2">Vote</span>
                    </label>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
          <div className="flex flex-row flex-no-wrap justify-between">
            <Link
              className="mt-2 bg-blue-500 rounded hover:bg-blue-700 text-white font-bold py-2 px-4 focus:outline-none focus:shadow-outline"
              to="/"
            >
              Home
            </Link>
            <button
              type="button"
              className="mt-2 bg-blue-500 rounded hover:bg-blue-700 text-white font-bold py-2 px-4 focus:outline-none focus:shadow-outline"
              disabled={!vote}
              onClick={() => postChoice(question.url, vote)}
            >
              Save vote
            </button>
          </div>
        </>
      ) : null}
    </div>
  );
};

export default QuestionDetail;
