import React from 'react';
import { render, wait, fireEvent } from '@testing-library/react';
import { useRouteMatch } from 'react-router';
import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';
import QuestionDetails from '../QuestionDetails';
import { PollsContext } from '../..';
import questionsData from '../../../hooks/usePolls/questions.json';

jest.mock('react-router', () => ({
  ...jest.requireActual('react-router'),
  useRouteMatch: jest.fn(),
}));

describe('<QuestionDetails />', () => {
  const history = createMemoryHistory();
  const value = {
    questions: questionsData,
    getQuestion: () => new Promise((resolve) => resolve(questionsData[0])),
    postChoice: jest.fn(),
  };
  const renderComponent = () =>
    render(
      <PollsContext.Provider value={value}>
        <Router history={history}>
          <QuestionDetails />
        </Router>
      </PollsContext.Provider>
    );

  beforeEach(() => {
    useRouteMatch.mockReturnValue({ params: { questionId: '1' } });
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  test('renders loading screen', () => {
    const { getByText } = renderComponent();
    expect(getByText('Question Details')).toBeInTheDocument();
    expect(getByText('Loading...')).toBeInTheDocument();
  });

  test('renders question details', async () => {
    const { getByText } = renderComponent();
    await wait(() => {
      expect(getByText('Most popular programming language?')).toBeInTheDocument();
    });
  });

  test('home button works', async () => {
    const { getByText } = renderComponent();
    await wait(() => {
      expect(getByText('Home')).toBeInTheDocument();
    });
    const historySpy = jest.spyOn(history, 'push');
    fireEvent.click(getByText('Home'));
    expect(historySpy).toHaveBeenCalledWith('/');
  });

  test('saves vote', async () => {
    const { getByText, container } = renderComponent();
    await wait(() => {
      expect(getByText('Question Details')).toBeInTheDocument();
    });
    const voteInput = container.querySelector('input[value=Go]');
    const voteButton = getByText('Save vote');
    fireEvent.click(voteInput);
    fireEvent.click(voteButton);
    expect(value.postChoice).toHaveBeenCalledTimes(1);
    expect(value.postChoice).toHaveBeenCalledWith('/questions/2', '/questions/2/choices/6');
  });
});
