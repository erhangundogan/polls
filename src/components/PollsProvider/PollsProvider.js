import React, { createContext } from 'react';
import usePolls from '../../hooks/usePolls/usePolls';

export const PollsContext = createContext({});

const PollsProvider = ({ children }) => {
  const { questions, setQuestions, getQuestion, getQuestions, postChoice } = usePolls();

  return (
    <PollsContext.Provider value={{ questions, setQuestions, getQuestion, getQuestions, postChoice }}>
      {children}
    </PollsContext.Provider>
  );
};

export default PollsProvider;
