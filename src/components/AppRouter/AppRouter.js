import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { QuestionsList, QuestionDetails } from '../';

const AppRouter = () => (
  <Router>
    <Switch>
      <Route exact path="/" component={QuestionsList} />
      <Route path="/questions/:questionId" component={QuestionDetails} />
    </Switch>
  </Router>
);

export default AppRouter;
