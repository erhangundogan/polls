import React from 'react';
import { render } from '@testing-library/react';
import QuestionsList from '../QuestionsList';
import { PollsContext } from '../..';
import questionsData from '../../../hooks/usePolls/questions.json';

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  Link: ({ children }) => children,
}));

describe('<QuestionsList />', () => {
  const renderComponent = () =>
    render(
      <PollsContext.Provider value={{ questions: questionsData }}>
        <QuestionsList />
      </PollsContext.Provider>
    );

  test('renders properly', () => {
    const { getByText, container } = renderComponent();
    expect(getByText('Questions')).toBeInTheDocument();
    expect(container.querySelectorAll('li')).toHaveLength(2);
  });
});
