import React, { useContext } from 'react';
import { Question, PollsContext } from '../';
import useOffline from '../../hooks/useOffline/useOffline';

const QuestionsList = () => {
  const isOffline = useOffline();
  const { questions } = useContext(PollsContext);

  return (
    <div className="questions w-full max-w p-2">
      <h1 className="text-2xl">Questions</h1>
      <h3 className="text-gray-600 mt-2 mb-4">Running {isOffline ? 'offline' : 'online'}</h3>
      <ul className="questions-list flex flex-row flex-wrap">
        {questions.map((item) => (
          <li className="w-full lg:w-3/12 mb-2 lg:mr-2" key={item.url}>
            <Question {...{ ...item }} />
          </li>
        ))}
      </ul>
    </div>
  );
};

export default QuestionsList;
