import axios from 'axios';
import { useEffect, useState } from 'react';
import questionsData from './questions.json';
import useOffline from '../useOffline/useOffline';

const URL = 'https://polls.apiblueprint.org';

const usePolls = () => {
  const isOffline = useOffline();
  const [questions, setQuestions] = useState(isOffline ? questionsData : []);

  useEffect(() => {
    if (!isOffline) {
      getQuestions().then((q) => setQuestions(q));
    }
  }, [isOffline]);

  const getQuestions = async () => {
    const { data } = await axios.get(`${URL}/questions`);
    return data;
  };

  const getQuestion = async (questionId) => {
    if (isOffline) {
      const [data] = questionsData.filter((q) => q.url === questionId);
      return await data;
    }
    const { data } = await axios.get(`${URL}${questionId}`);
    return data;
  };

  const postChoice = async (questionUrl, choiceUrl) => {
    if (isOffline) {
      const currentQuestions = [...questions];
      currentQuestions.forEach((question) => {
        if (question.url === questionUrl) {
          question.choices.forEach((choice) => {
            if (choice.url === choiceUrl) {
              choice.votes += 1;
            }
          });
        }
      });
      setQuestions(currentQuestions);
      return;
    }
    const { data } = await axios.post(`${URL}${choiceUrl}`);
    return data;
  };

  return {
    questions,
    setQuestions,
    getQuestion,
    getQuestions,
    postChoice,
  };
};

export default usePolls;
