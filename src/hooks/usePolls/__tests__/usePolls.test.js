import axios from 'axios';
import { renderHook, act } from '@testing-library/react-hooks';
import usePolls from '../usePolls';
import useOffline from '../../useOffline/useOffline';
import questionsData from '../questions.json';

jest.mock('axios');
jest.mock('../../useOffline/useOffline');

describe('usePolls', () => {
  let hook = null;

  const setup = () => renderHook(() => usePolls());

  beforeEach(() => {
    useOffline.mockReturnValue(false);
    axios.get.mockResolvedValue({ data: [] });
  });

  test('returns specific object', () => {
    const { result } = setup();

    expect(result.current).toMatchObject({
      questions: [],
      setQuestions: expect.any(Function),
      getQuestion: expect.any(Function),
      getQuestions: expect.any(Function),
      postChoice: expect.any(Function),
    });
  });

  describe('offline', () => {
    beforeEach(() => {
      act(() => {
        useOffline.mockReturnValue(true);
      });
      hook = setup();
    });

    test('getQuestion works', async () => {
      const response = await hook.result.current.getQuestion('/questions/2');
      expect(response).toEqual(questionsData[0]);
    });

    test('postChoice works', async () => {
      expect(hook.result.current.questions[0].choices[3].votes).toBe(3);
      await hook.result.current.postChoice('/questions/2', '/questions/2/choices/7');
      expect(hook.result.current.questions[0].choices[3].votes).toBe(4);
    });
  });

  describe('online', () => {
    beforeEach(() => {
      act(() => {
        useOffline.mockReturnValue(false);
      });
      hook = setup();
    });

    afterEach(() => jest.resetAllMocks());

    test('getQuestion works', async () => {
      axios.get.mockResolvedValueOnce({ data: [questionsData[0]] });
      await hook.result.current.getQuestion('/questions/2');
      expect(axios.get).toHaveBeenCalledWith('https://polls.apiblueprint.org/questions/2');
    });

    test('postChoice works', async () => {
      axios.post.mockResolvedValueOnce({ data: {} });
      await hook.result.current.postChoice('/questions/2', '/questions/2/choices/6');
      expect(axios.post).toHaveBeenCalledWith('https://polls.apiblueprint.org/questions/2/choices/6');
    });
  });
});
