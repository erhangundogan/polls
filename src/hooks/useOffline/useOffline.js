import { useState, useEffect } from 'react';

const useOffline = () => {
  const [offline, setOffline] = useState(() => !window.navigator.onLine);

  useEffect(() => {
    const offlineHandler = window.addEventListener('offline', () => {
      console.log('Gone offline:', Date());
      setOffline(true);
    });
    const onlineHandler = window.addEventListener('online', () => {
      console.log('Gone online:', Date());
      setOffline(false);
    });

    return () => {
      window.removeEventListener('offline', offlineHandler);
      window.removeEventListener('online', onlineHandler);
    };
  }, []);

  return offline;
};

export default useOffline;
