import { renderHook } from '@testing-library/react-hooks';
import useOffline from '../useOffline';

describe('useOffline', () => {
  const setup = () => renderHook(() => useOffline());
  const addEventListener = jest.fn();
  const removeEventListener = jest.fn();

  beforeEach(() => {
    Object.defineProperty(window, 'addEventListener', {
      writable: true,
      configurable: true,
      value: addEventListener,
    });

    Object.defineProperty(window, 'removeEventListener', {
      writable: true,
      configurable: true,
      value: removeEventListener,
    });
  });

  afterEach(() => jest.clearAllMocks());

  test('returns true when offline', () => {
    Object.defineProperty(window, 'navigator', {
      writable: true,
      configurable: true,
      value: {
        onLine: false,
      },
    });
    const { result } = setup();

    expect(result.current).toBe(true);
  });

  test('returns false when online', () => {
    Object.defineProperty(window, 'navigator', {
      writable: true,
      configurable: true,
      value: {
        onLine: true,
      },
    });
    const { result } = setup();

    expect(result.current).toBe(false);
  });
});
